#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QString>
#include <string>
#include <typeinfo>
#include <type_traits>
using namespace std;


template<typename T>
void foo(T&& t_arg, char* t_outputArr) {
    const char *tempArray = typeid(t_arg).name();
    qDebug() << "Universal refference const T&& t_arg";
    int i;
    for(i = 0; i < 20; i++) {
        if (tempArray[i] == '\0')
            break;
        t_outputArr[i] = tempArray[i];
    }
    t_outputArr[i] = ' ';
    if (std::is_rvalue_reference<const T&&>::value)
        t_outputArr[i + 1] = 'R';
    else if (std::is_lvalue_reference<const T&&>::value)
        t_outputArr[i + 1] = 'L';
    else
        t_outputArr[i + 1] = '0';

    if (std::is_reference<const T&&>::value)
        t_outputArr[i + 2] = 'r';
    else if (std::is_pointer<const T&&>::value)
        t_outputArr[i + 2] = 'p';
    else
        t_outputArr[i + 2] = 'v';
    t_outputArr[i + 3] = '\0';
}
/*
template<typename T>
void foo(const T& t_arg, char* t_outputArr) {
    const char *tempArray = typeid(t_arg).name();
    qDebug() << " lvalue refference - const T&";
    int i;
    for(i = 0; i < 20; i++) {
        if (tempArray[i] == '\0')
            break;
        t_outputArr[i] = tempArray[i];
    }
    t_outputArr[i] = ' ';
    if (std::is_rvalue_reference<const T&>::value)
        t_outputArr[i + 1] = 'R';
    else if (std::is_lvalue_reference<const T&>::value)
        t_outputArr[i + 1] = 'L';
    else
        t_outputArr[i + 1] = '0';

    if (std::is_reference<const T&>::value)
        t_outputArr[i + 2] = 'r';
    else
        t_outputArr[i + 2] = 'v';
    t_outputArr[i + 3] = '\0';
}
*/

template<typename T>
void foo2(T t_arg, char* t_outputArr) {
    const char *tempArray = typeid(t_arg).name();
    qDebug() << " value - T";
    int i;
    for(i = 0; i < 20; i++) {
        if (tempArray[i] == '\0')
            break;
        t_outputArr[i] = tempArray[i];
    }
    t_outputArr[i] = ' ';
    if (std::is_rvalue_reference<T>::value)
        t_outputArr[i + 1] = 'R';
    else if (std::is_lvalue_reference<T>::value)
        t_outputArr[i + 1] = 'L';
    else
        t_outputArr[i + 1] = '0';

    if (std::is_reference<T>::value)
        t_outputArr[i + 2] = 'r';
    else if (std::is_pointer<T>::value)
        t_outputArr[i + 2] = 'p';
    else
        t_outputArr[i + 2] = 'v';
    t_outputArr[i + 3] = '\0';
}

template<typename T>
void foo3(std::initializer_list<T> t_arg, char* t_outputArr) {
    const char *tempArray = typeid(t_arg).name();
    qDebug() << " value - T";
    int i;
    for(i = 0; i < 20; i++) {
        if (tempArray[i] == '\0')
            break;
        t_outputArr[i] = tempArray[i];
    }
    t_outputArr[i] = ' ';
    if (std::is_rvalue_reference<T>::value)
        t_outputArr[i + 1] = 'R';
    else if (std::is_lvalue_reference<T>::value)
        t_outputArr[i + 1] = 'L';
    else
        t_outputArr[i + 1] = '0';

    if (std::is_reference<T>::value)
        t_outputArr[i + 2] = 'r';
    else if (std::is_pointer<T>::value)
        t_outputArr[i + 2] = 'p';
    else
        t_outputArr[i + 2] = 'v';
    t_outputArr[i + 3] = '\0';
}

struct S {
    static const int x = 14; // static data member
    // a definition outside of class is required if it is odr-used
};
//constexpr
template<typename T, size_t N>
constexpr std::size_t arraySize(T (&)[N]) noexcept {
    return N;
}

void twiceInt(int t_arg) {
    qDebug() << t_arg * 2;
}

void fooForward(void foo(int) ) {
    foo(5);
}

class Test {
public:
    Test(int t_i);
private:
    int m_i;
};

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    char typeName[40];
    qDebug() << "sizeOf typeName array" << arraySize(typeName) ;

    foo2(Test(5), typeName);
    qDebug() << typeName;

    Test testObj = Test(5);
    foo2(testObj, typeName);
    qDebug() << typeName;

    const Test &rtestObj = testObj;
    foo(rtestObj, typeName);
    qDebug() << typeName;

    const Test * const ptestObj = &testObj;
    foo2(ptestObj, typeName);
    qDebug() << typeName;

    //test how poinerts to function works
    fooForward(twiceInt);

    //test auto type deduction with std::initializer_list
    foo3({12, 11, 10} , typeName);
    qDebug() << typeName << "foo3(std::initializer_list<T> t_arg";

    //test lambdas
    int n = (S::x);  // S::x is odr-used here: a definition is required
    qDebug() << n;
    return app.exec();
}


Test::Test(int t_i) {
    m_i = t_i;
    qDebug() << "Test::Test";
}
